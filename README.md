<p><img  src="./logo.png" height="130px" alt="Paladium"></p>

<h1>Paladium Launcher</h1>

<p>
    <img src="https://img.shields.io/badge/build-passing-dark_green.svg?style=for-the-badge" alt="build">
    <img src="https://img.shields.io/badge/version-1.0.0--beta.4(0.0.01--d18)-dark_green.svg?style=for-the-badge" alt="version">
</p>

---

## Développement

**Configuration requise**

* [Node.js][nodejs] v12.x.x
* [Git][git]

---

**Cloner et installer les dépendances**

```console
$ git clone http://ssh.palagitium.dev/PalaDev-Web/paladiumlauncher.git
$ cd paladiumlauncher
$ npm install
```

---

**Lancer l'application**

```console
$ npm start
```

---

**Console**

Pour ouvrir la console, utilisez le raccourci clavier suivant :

```console
ctrl + shift + i
```

---

**Installateurs**

Build pour sa plateforme :

```console
$ npm run dist
```

Build pour une plateforme spécifique :

| Platform    | Command              |
| ----------- | -------------------- |
| Windows x64 | `npm run dist:win`   |
| macOS       | `npm run dist:mac`   |
| Linux x64   | `npm run dist:linux` |

---

#### Contributeurs
<p>
    <a href="https://palagitium.dev/Bestdevelop"><img width="80" src="https://palagitium.dev/uploads/-/system/user/avatar/5/avatar.png" alt="Chaika9"></a>
    <a href="https://palagitium.dev/Faustin"><img width="80" src="https://www.gravatar.com/avatar/321b531e47762a7c6eb21a05f1ec95f4?s=180&d=identicon" alt="Faustin"></a>
</p>

---
Copyright 2020 Paladium. All rights reserved.

[nodejs]: https://nodejs.org/en/ 'Node.js'
[git]: https://git-scm.com/ 'Git'